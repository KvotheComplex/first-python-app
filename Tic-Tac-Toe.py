'''Simple Tic-Tac-Toe Game'''
import time
import os
import sys

#Global variables
player_one_turn = True
player1 = ""
player2 = ""
marker = ""
selected_numbers = []

# Get the width of the terminal
terminal_width = os.get_terminal_size().columns

#Reset the display
def reset_display():
    global row1, lne1,blk1,row2,lne2,blk2,row3

    row1 = "   1   |   2   |   3   |   "
    lne1 = "___________________________"
    blk1 = "                           "
    row2 = "   4   |   5   |   6   |   "
    lne2 = "___________________________"
    blk2 = "                           "
    row3 = "   7   |   8   |   9   |   "

#center any print statements

def center_text(text, width):
    return text.center(width)

#center any input prompts

def center_input(prompt, width):
    return input(center_text(prompt, width))

#clear the screen

def clear_screen():
    if os.name == 'posix':  # Linux and macOS
        os.system('clear')
    elif os.name == 'nt':  # Windows
        os.system('cls')

def reset_turns():
    global player_one_turn
    player_one_turn = True

def clear_selected_numbers():
    global selected_numbers
    selected_numbers = []

def change_turn():
    global player_one_turn
    player_one_turn = not player_one_turn

#Begin application and set player names

def setup():
    global player1, player2
    while True:
        print(center_text("Welcome to the table! Would you like to play a game of tic-tac-toe?", terminal_width))
        start_game = center_input("Start new game?\n", terminal_width)
        print(center_text("", terminal_width))
        if start_game.lower() in ["yes", "y"]:
            break
        if start_game.lower() in ["no", "n"]:
            print(center_text("Ok, see you next time!", terminal_width))
            time.sleep(1)
            exit()
        else:
            print(center_text("Invalid input. Please type yes or no.", terminal_width))
    while True:
        clear_screen()
        print(center_text("Ok please enter a name for player one", terminal_width))
        player1 = center_input("\n",terminal_width)
        print(center_text("\n", terminal_width))
        print(center_text(f'Ok so {player1} is player one. Is this correct', terminal_width))
        print(center_text("\n", terminal_width))
        response = center_input("Please type yes or no\n", terminal_width)

        if response.lower() in ["yes", "y"]:
            print(center_text("", terminal_width))
            break
        elif response.lower() in ["no", "n"]:
            continue
        else:
            print(center_text("Invalid input. Please re-enter and then validate name for player one.", terminal_width))
    while True:
        clear_screen()
        print(center_text("Ok please enter a name for player two", terminal_width))
        player2 = center_input("\n", terminal_width)
        print(center_text("\n", terminal_width))
        print(center_text(f'Ok so {player2} is player two. Is this correct', terminal_width))
        print(center_text("\n", terminal_width))
        response2 = center_input("Please type yes or no\n", terminal_width)

        if response2.lower() in ["yes", "y"]:
            print(center_text("\n", terminal_width))
            clear_screen()
            print(center_text("Ok I will now set up the board for you", terminal_width))
            time.sleep(2)
            clear_screen()
            reset_display()
            break
        if response2.lower() in ["no", "n"]:
            continue

        print(center_text("Invalid input. Please re-enter and validate name for player two", terminal_width))

#Create the centered display

def draw_display(row1, row2, row3):

    print("\n" * 5)

    print(row1.center(terminal_width))
    print(lne1.center(terminal_width))
    print(blk1.center(terminal_width))
    print(row2.center(terminal_width))
    print(lne2.center(terminal_width))
    print(blk2.center(terminal_width))
    print(row3.center(terminal_width))
    print(center_text("", terminal_width) * 2)

def update_display(selection, marker):
    global row1, row2, row3

    if selection == 1:
        row1 = row1[:3] + marker + row1[4:]
    elif selection == 2:
        row1 = row1[:11] + marker + row1[12:]
    elif selection == 3:
        row1 = row1[:19] + marker + row1[20:]
    if selection == 4:
        row2 = row2[:3] + marker + row2[4:]
    elif selection == 5:
        row2 = row2[:11] + marker + row2[12:]
    elif selection == 6:
        row2 = row2[:19] + marker + row2[20:]
    if selection == 7:
        row3 = row3[:3] + marker + row3[4:]
    elif selection == 8:
        row3 = row3[:11] + marker + row3[12:]
    elif selection == 9:
        row3 = row3[:19] + marker + row3[20:]
    clear_screen()
    draw_display(row1, row2, row3)
    result = check_winner()
    selected_numbers.sort()
    if result is True and player_one_turn:
        print(center_text(F"Congratulations {player1} you won!", terminal_width))
        print(center_text("\n", terminal_width))
        again = center_input("Would you like to play again?\n", terminal_width)
        if again.lower() in ["yes", "y"]:
            print(center_text("\n", terminal_width))
            print(center_text("Ok let me reset the board for you", terminal_width))
            time.sleep(2)
            reset_turns()
            clear_selected_numbers()
            clear_screen()
            reset_display()
            draw_display(row1, row2, row3)
            start()
        elif again.lower() in ["no", "n"]:
            print(center_text("\n", terminal_width))
            print(center_text("Ok thanks for playing!", terminal_width))
            time.sleep(2)
            sys.exit()
        else:
            print(center_text("\n", terminal_width))
            print(center_text("Invalid input. Please type yes or no", terminal_width))
    elif result is True and not player_one_turn:
        print(center_text(F"Congratulations {player2} you won!", terminal_width))
        print(center_text("\n", terminal_width))
        again = center_input("Would you like to play again?\n", terminal_width)
        if again.lower() in ["yes", "y"]:
            print(center_text("\n", terminal_width))
            print(center_text("Ok let me reset the board for you", terminal_width))
            time.sleep(2)
            reset_turns()
            clear_selected_numbers()
            clear_screen()
            reset_display()
            draw_display(row1, row2, row3)
            start()
        elif again.lower() in ["no", "n"]:
            print(center_text("\n", terminal_width))
            print(center_text("Ok thanks for playing!", terminal_width))
            time.sleep(2)
            sys.exit()
        else:
            print(center_text("\n", terminal_width))
            print(center_text("Invalid input. Please type yes or no", terminal_width))
    elif selected_numbers == [1, 2, 3, 4, 5, 6, 7, 8, 9] and result == False:
        print(center_text("MEOW! CATSCRATCH!", terminal_width))
        print(center_text("\n", terminal_width))
        again = center_input("Would you like to play again?\n", terminal_width)
        if again.lower() in ["yes", "y"]:
            print(center_text("\n", terminal_width))
            print(center_text("Ok let me reset the board for you", terminal_width))
            time.sleep(2)
            reset_turns()
            clear_selected_numbers()
            clear_screen()
            reset_display()
            draw_display(row1, row2, row3)
            start()
        elif again.lower() in ["no", "n"]:
            print(center_text("\n", terminal_width))
            print(center_text("Ok thanks for playing!", terminal_width))
            time.sleep(2)
            sys.exit()
    else:
        pass
    change_turn()
    start()


def start():
    global marker
    while player_one_turn:
        marker = "X"
        print(center_text(F"{player1} please select a position to place an {marker}", terminal_width))
        selection = center_input("Please enter a number\n", terminal_width)
        if selection.isdigit():
            selection = int(selection)
            if selection in range(1,10) and selection not in selected_numbers:
                selected_numbers.append(selection)
                update_display(int(selection), marker)
                break
            if selection not in range(1,10):
                center_input("Invalid input. please enter a number\n", terminal_width)
            elif selection in selected_numbers:
                center_input("Please enter a number that has not been selected previously\n", terminal_width)
        else:
            print(center_input("Invalid response. Please enter a number\n", terminal_width))
    while not player_one_turn:
        marker = "O"
        print(center_text(F"{player2} please select a position to place an {marker}", terminal_width))
        selection = center_input("Please enter a number\n", terminal_width)
        if selection.isdigit():
            selection = int(selection)
            if selection in range(1,10) and selection not in selected_numbers:
                selected_numbers.append(selection)
                update_display(int(selection), marker)
                break
            if selection not in range(1,10):
                center_input("Invalid input. please enter a number\n", terminal_width)
            if selection in selected_numbers:
                center_input("Please enter a number that has not been selected previously\n", terminal_width)
        else:
            print(center_input("Invalid response. Please enter a number\n", terminal_width))

def check_winner():
    global row1, row2, row3
    
    #Check rows
    if row1[3] == row1[11] == row1[19]:
        return True
    if row2[3] == row2[11] == row2[19]:
        return True
    if row3[3] == row3[11] == row3[19]:
        return True
    
    #Check Columns
    if row1[3] == row2[3] == row3[3]:
        return True
    if row1[11] == row2[11] == row3[11]:
        return True
    if row1[19] == row2[19] == row3[19]:
        return True
    
    #Check Diagonals
    if row1[3] == row2[11] == row3[19]:
        return True
    if row1[19] == row2[11] == row3[3]:
        return True

    return False


setup()
draw_display(row1, row2, row3)
start()
